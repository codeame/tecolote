# -*- coding: utf-8 -*-

# Tecolote Server.
# A GPS Tracking Data-Collecting Server.
#
# (C) 2012  Miguel Chavez Gamboa.
# miguel@codea.me
#

from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor

import protocols
from protocols.receiver import *

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("port", help="Run on the specified port", type=int)
args = parser.parse_args()

print 'TECOLOTE SERVER Running on port %s'%args.port

endpoint = TCP4ServerEndpoint(reactor, args.port) #int(os.environ['PORT'])
endpoint.listen( TrackerReceiverFactory() )
reactor.run()

