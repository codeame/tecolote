Comandos implementados:

Recepcion:
    Login
    IMEI_&_SN *
    Reporte de Posicion
    Reporte de Alarmas con posicion (diferentes alarmas en el mismo comando).

Envio ** :
    Login Confirmation
    Get IMEI_&_SN
    Set Heading Change


*  : No se ha probado con el dispositivo, ya que no ha enviado ese comando pese a haber enviado la solicitud.
** : No se ha recibido ninguna confirmacion/error del dispositivo al enviar los comandos (como si sucede por sms).
     Sin embargo, la primera vez que se envio el comando de HeadingChange, el dispositivo comenzo a enviar las alarmas de viraje.
     Pero no reporta todas las vueltas que deberia (solo reporta algunas, y recientemente ninguna.)
     Tambien, presionando el boton de panico, el dispositivo envia la alarma con el tipo input1/(SOS button). Algunas veces repetido.





-----------------------------------------------------------------------------------------------------
Pruebas de Stress:

Se probo una rafaga de comandos enviados uno tras otro al servidor.
El server, usa alrededor del 50% del CPU (dos nucleos), y 10~13 MB de Memoria.

Con 1000, se tarda 30 segundos en procesarlos (envio/recepcion).
A los 761, empieza a haber timeouts, por lo tanto no se recibe una respuesta del server (esta saturado).



------------------------------------------------------------------------------------------------------

Pruebas para el receiver.

Login:
2424001113612345678fff500005d80d0a
24 24 00 11 12 34 56 FF FF FF FF 50 00 8B 9B 0D 0A

$$001113612345678fff500005d8


GET IMEI & SN:

Cmd: 
24240033123456FFFFFFFF9001333033373831353035392C3335333335383031313031383134352C56312E34352D4EFED00d0a

Response DATA:
333033373831353035392C3335333335383031313031383134352C56312E34352D4E




NOTAS:-------------------------------------------------------

    Hay reportes que llegan con fecha del pasado. 
    Y Curiosamente, llega el mismo reporte muchas veces (misma fecha-hora, posicion, heading, velocidad,etc..)
    Tal vez un reporte que se quedo colgado (idle) o que no se logro conectar (despues de un crash o no estaba corriendo)
    y se sigue intentando. Sin embargo si se recive muchas veces repetido sin que quede idle|sin-conexion.

========================================================================================================================================
Con TZ=FALSE

Adding poi: [150] 2012-10-25 00:15:53 / 2012-10-25 03:29:28.205878 -- 0.00km/h - 0.0 °
Adding poi: [105] 2012-10-25 00:36:08 / 2012-10-25 00:52:53.692210 -- 0.00km/h - 340.0 °
Adding poi: [206] 2012-10-25 00:56:18 / 2012-10-25 04:37:58.169953 -- 0.00km/h - 15.0 °
Adding poi: [110] 2012-10-25 01:16:30 / 2012-10-25 01:16:39.970757 -- 0.00km/h - 0.0 °
Adding poi: [116] 2012-10-25 01:36:42 / 2012-10-25 01:44:54.487807 -- 0.11km/h - 3.0 °
Adding poi: [125] 2012-10-25 01:56:53 / 2012-10-25 01:57:00.404925 -- 0.00km/h - 43.0 °
Adding poi: [130] 2012-10-25 03:03:06 / 2012-10-25 03:07:53.909060 -- 0.00km/h - 43.0 °
Adding poi: [138] 2012-10-25 03:13:58 / 2012-10-25 03:19:05.037277 -- 7.04km/h - 310.0 °
Adding poi: [145] 2012-10-25 03:23:18 / 2012-10-25 03:23:32.297336 -- 0.00km/h - 197.0 °
Adding poi: [160] 2012-10-25 03:43:29 / 2012-10-25 03:44:25.933857 -- 0.00km/h - 15.0 °
Adding poi: [174] 2012-10-25 03:56:40 / 2012-10-25 04:00:39.301224 -- 9.20km/h - 162.0 °
Adding poi: [184] 2012-10-25 03:56:59 / 2012-10-25 04:12:14.083276 -- 6.24km/h - 320.0 °
Adding poi: [190] 2012-10-25 04:03:41 / 2012-10-25 04:19:16.731574 -- 0.00km/h - 162.0 °
Adding poi: [195] 2012-10-25 04:23:51 / 2012-10-25 04:25:12.463058 -- 0.00km/h - 162.0 °
Adding poi: [213] 2012-10-25 04:44:03 / 2012-10-25 04:46:04.580203 -- 0.00km/h - 15.0 °
Adding poi: [229] 2012-10-25 05:04:14 / 2012-10-25 05:04:26.437443 -- 0.00km/h - 15.0 °
Adding poi: [242] 2012-10-25 17:31:07 / 2012-10-25 17:31:18.187620 -- 0.00km/h - 0.0 °
Adding poi: [262] 2012-10-25 17:51:22 / 2012-10-25 17:54:17.246908 -- 0.00km/h - 58.0 °
Adding poi: [283] 2012-10-25 18:11:33 / 2012-10-25 18:20:12.414191 -- 0.00km/h - 15.0 °
Adding poi: [299] 2012-10-25 18:31:45 / 2012-10-25 18:39:45.383183 -- 0.00km/h - 15.0 °
Adding poi: [302] 2012-10-25 18:39:27 / 2012-10-25 18:43:18.204455 -- 8.80km/h - 62.0 °
Adding poi: [311] 2012-10-25 18:43:35 / 2012-10-25 18:53:37.485355 -- 6.24km/h - 13.0 °
Adding poi: [317] 2012-10-25 18:51:56 / 2012-10-25 19:00:30.672245 -- 0.00km/h - 349.0 °
Adding poi: [334] 2012-10-25 19:12:07 / 2012-10-25 19:19:59.889031 -- 0.00km/h - 15.0 °
Adding poi: [351] 2012-10-25 19:32:19 / 2012-10-25 19:40:46.564756 -- 0.00km/h - 15.0 °
Adding poi: [365] 2012-10-25 19:52:30 / 2012-10-25 19:56:50.922811 -- 0.00km/h - 15.0 °
Adding poi: [374] 2012-10-25 21:49:45 / 2012-10-25 21:51:39.724235 -- 0.00km/h - 0.0 °
Adding poi: [392] 2012-10-25 22:09:58 / 2012-10-25 22:14:03.867224 -- 0.00km/h - 15.0 °
Adding poi: [408] 2012-10-25 22:30:10 / 2012-10-25 22:33:53.937334 -- 0.00km/h - 15.0 °
Adding poi: [422] 2012-10-25 22:50:20 / 2012-10-25 22:50:30.416086 -- 0.00km/h - 340.0 °


Con TZ=TRUE

Adding poi: [150] 2012-10-25 05:15:53+00:00 / 2012-10-25 08:29:28.205878+00:00 -- 0.00km/h - 0.0 °
Adding poi: [105] 2012-10-25 05:36:08+00:00 / 2012-10-25 05:52:53.692210+00:00 -- 0.00km/h - 340.0 °
Adding poi: [369] 2012-10-25 05:56:18+00:00 / 2012-10-26 01:02:51.393778+00:00 -- 0.00km/h - 15.0 °
Adding poi: [110] 2012-10-25 06:16:30+00:00 / 2012-10-25 06:16:39.970757+00:00 -- 0.00km/h - 0.0 °
Adding poi: [116] 2012-10-25 06:36:42+00:00 / 2012-10-25 06:44:54.487807+00:00 -- 0.11km/h - 3.0 °
Adding poi: [125] 2012-10-25 06:56:53+00:00 / 2012-10-25 06:57:00.404925+00:00 -- 0.00km/h - 43.0 °
Adding poi: [128] 2012-10-25 08:03:06+00:00 / 2012-10-25 08:05:37.397633+00:00 -- 0.00km/h - 43.0 °
Adding poi: [138] 2012-10-25 08:13:58+00:00 / 2012-10-25 08:19:05.037277+00:00 -- 7.04km/h - 310.0 °
Adding poi: [145] 2012-10-25 08:23:18+00:00 / 2012-10-25 08:23:32.297336+00:00 -- 0.00km/h - 197.0 °
Adding poi: [160] 2012-10-25 08:43:29+00:00 / 2012-10-25 08:44:25.933857+00:00 -- 0.00km/h - 15.0 °
Adding poi: [171] 2012-10-25 08:56:40+00:00 / 2012-10-25 08:57:10.580809+00:00 -- 9.20km/h - 162.0 °
Adding poi: [181] 2012-10-25 08:56:59+00:00 / 2012-10-25 09:08:46.736627+00:00 -- 6.24km/h - 320.0 °
Adding poi: [191] 2012-10-25 09:03:41+00:00 / 2012-10-25 09:20:36.711513+00:00 -- 0.00km/h - 162.0 °
Adding poi: [202] 2012-10-25 09:23:51+00:00 / 2012-10-25 09:33:16.031058+00:00 -- 0.00km/h - 162.0 °
Adding poi: [216] 2012-10-25 09:44:03+00:00 / 2012-10-25 09:49:32.142896+00:00 -- 0.00km/h - 15.0 °
Adding poi: [232] 2012-10-25 10:04:14+00:00 / 2012-10-25 10:07:52.774743+00:00 -- 0.00km/h - 15.0 °
Adding poi: [248] 2012-10-25 22:31:07+00:00 / 2012-10-25 22:38:12.946508+00:00 -- 0.00km/h - 0.0 °
Adding poi: [265] 2012-10-25 22:51:22+00:00 / 2012-10-25 22:58:16.402881+00:00 -- 0.00km/h - 58.0 °
Adding poi: [276] 2012-10-25 23:11:33+00:00 / 2012-10-25 23:12:02.936133+00:00 -- 0.00km/h - 15.0 °
Adding poi: [298] 2012-10-25 23:31:45+00:00 / 2012-10-25 23:38:36.341859+00:00 -- 0.00km/h - 15.0 °
Adding poi: [307] 2012-10-25 23:39:27+00:00 / 2012-10-25 23:49:01.706091+00:00 -- 8.80km/h - 62.0 °
Adding poi: [312] 2012-10-25 23:43:35+00:00 / 2012-10-25 23:54:46.873285+00:00 -- 6.24km/h - 13.0 °
Adding poi: [319] 2012-10-25 23:51:56+00:00 / 2012-10-26 00:02:48.571749+00:00 -- 0.00km/h - 349.0 °


=================================================== GPS ================================================================================

Connection from IPv4Address(TCP, '189.254.205.154', 51632), 4 peers connected, 4 peers attended.
Data Received: $$E" ��U210705.000,A,1915.0624,N,10343.7340,W,0.00,189,131012,,*1D|0.9|533|2000|000A,0009|014E0014084940A7|0B|00000018

Data Received: $$E" �PNQ




Connection from IPv4Address(TCP, '200.95.171.13', 34968), 2 peers connected, 2 peers attended.
Data Received - Raw: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?�<ENTER> | upper: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?

Line received: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?� [from IPv4Address(TCP, '200.95.171.13', 34968)]
Not a valid data. Missing starting $$.



Connection from IPv4Address(TCP, '200.95.171.137', 55701), 3 peers connected, 3 peers attended.
Data Received - Raw: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?�<ENTER> | upper: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?

Line received: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?� [from IPv4Address(TCP, '200.95.171.137', 55701)]
Not a valid data. Missing starting $$.


Connection from IPv4Address(TCP, '200.95.171.12', 26218), 4 peers connected, 4 peers attended.
Data Received - Raw: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?�<ENTER> | upper: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?

Line received: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?� [from IPv4Address(TCP, '200.95.171.12', 26218)]
Not a valid data. Missing starting $$.


Connection from IPv4Address(TCP, '200.95.171.17', 29267), 5 peers connected, 5 peers attended.
Data Received - Raw: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?�<ENTER> | upper: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?

Line received: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?� [from IPv4Address(TCP, '200.95.171.17', 29267)]
Not a valid data. Missing starting $$.
Connection from IPv4Address(TCP, '189.254.205.148', 30374), 6 peers connected, 6 peers attended.
Data Received - Raw: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?�<ENTER> | upper: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?

Line received: $$E" ��U212717.000,A,1915.0585,N,10343.7319,W,0.00,102,131012,,*1B|0.9|532|2000|0009,0008|014E0014084940A7|09|0000002B?� [from IPv4Address(TCP, '189.254.205.148', 30374)]
Not a valid data. Missing starting $$.




Connection from IPv4Address(TCP, '200.95.171.133', 53728), 8 peers connected, 8 peers attended.
Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER> | upper: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�

Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '200.95.171.133', 53728)]
Not a valid data. Missing starting $$.
Connection from IPv4Address(TCP, '200.95.171.12', 51094), 9 peers connected, 9 peers attended.
Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER> | upper: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�

Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '200.95.171.12', 51094)]
Not a valid data. Missing starting $$.
Connection from IPv4Address(TCP, '200.95.171.138', 56161), 10 peers connected, 10 peers attended.
Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER> | upper: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�

Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '200.95.171.138', 56161)]
Not a valid data. Missing starting $$.
Connection from IPv4Address(TCP, '189.254.205.154', 24568), 11 peers connected, 11 peers attended.


Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER> | upper: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�

Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '189.254.205.154', 24568)]
Not a valid data. Missing starting $$.












Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER>
Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '200.95.171.137', 28920)]
Error on value, expected a number. Cannot continue...


Connection from IPv4Address(TCP, '200.95.171.137', 56138), 1 peers connected, 2 peers attended.
Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER>
Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '200.95.171.137', 56138)]
Error on value, expected a number. Cannot continue..


Connection from IPv4Address(TCP, '200.95.171.146', 33865), 1 peers connected, 3 peers attended.
Data Received - Raw: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041��<ENTER>
Line received: $$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�� [from IPv4Address(TCP, '200.95.171.146', 33865)]
Error on value, expected a number. Cannot continue...












Raw Data:$$~E" ��U230813.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*21|0.9|486|2000|0009,0008|014E0014084940A7|0C|0000007F�

Raw Data:$$~E" ��U214728.000,A,1915.0508,N,10343.7316,W,0.00,41,131012,,*2D|1.2|536|2000|000A,0008|014E0014084940A7|0B|00000041�

Raw Data:$$~E" ��U220740.000,A,1915.0502,N,10343.7292,W,0.00,41,131012,,*23|0.8|575|2000|0009,0008|014E0014084940A7|0C|00000064�

Raw Data:$$~E" ��U224803.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*25|0.9|486|2000|000A,0008|014E0014084940A7|0C|0000007F�:

Raw Data:$$~E" ��U220740.000,A,1915.0502,N,10343.7292,W,0.00,41,131012,,*23|0.8|575|2000|0009,0008|014E0014084940A7|0C|00000064�

Raw Data:$$E" �PnQ








Connection from IPv4Address(TCP, '200.95.171.17', 48257), 1 peers connected, 1 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '201.144.60.165', 30287), 1 peers connected, 2 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.14', 32874), 1 peers connected, 3 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.12', 23042), 1 peers connected, 4 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '201.144.60.171', 31052), 1 peers connected, 5 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.134', 50729), 1 peers connected, 6 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '201.144.60.165', 28110), 1 peers connected, 7 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '201.144.60.168', 33596), 1 peers connected, 8 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.12', 32143), 1 peers connected, 9 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.133', 29532), 1 peers connected, 10 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.21', 57514), 1 peers connected, 11 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.134', 26713), 1 peers connected, 12 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.134', 32037), 1 peers connected, 13 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.146', 51747), 1 peers connected, 14 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�



Connection from IPv4Address(TCP, '189.254.205.153', 23396), 1 peers connected, 15 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.146', 31160), 1 peers connected, 16 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.23', 30381), 1 peers connected, 17 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '200.95.171.137', 58346), 1 peers connected, 18 peers attended.
Raw Data:$$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(�

Connection from IPv4Address(TCP, '189.254.205.153', 24958), 1 peers connected, 19 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '189.254.205.153', 32271), 1 peers connected, 20 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '200.95.171.146', 52695), 1 peers connected, 21 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '201.144.60.171', 50053), 1 peers connected, 22 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '200.95.171.134', 32676), 1 peers connected, 23 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '200.95.171.12', 56577), 1 peers connected, 24 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '200.95.171.23', 48219), 1 peers connected, 25 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ

Connection from IPv4Address(TCP, '200.95.171.138', 54441), 1 peers connected, 26 peers attended.
Raw Data:$$~E" ��U234837.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*23|0.8|486|2000|000A,0008|014E001408497A94|0D|0000007FJ


for d in data:
  print ord(d)

$=> dec:36 | hex: 0x24
$=> dec:36 | hex: 0x24
=> dec:0 | hex: 0x0
~=> dec:126 | hex: 0x7e
E=> dec:69 | hex: 0x45
"=> dec:34 | hex: 0x22
=> dec:21 | hex: 0x15
 => dec:32 | hex: 0x20
=> dec:24 | hex: 0x18
=> dec:127 | hex: 0x7f
�=> dec:255 | hex: 0xff
�=> dec:153 | hex: 0x99
U=> dec:85 | hex: 0x55
2=> dec:50 | hex: 0x32
3=> dec:51 | hex: 0x33
2=> dec:50 | hex: 0x32
8=> dec:56 | hex: 0x38
2=> dec:50 | hex: 0x32
5=> dec:53 | hex: 0x35
.=> dec:46 | hex: 0x2e
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
,=> dec:44 | hex: 0x2c
A=> dec:65 | hex: 0x41
,=> dec:44 | hex: 0x2c
1=> dec:49 | hex: 0x31
9=> dec:57 | hex: 0x39
1=> dec:49 | hex: 0x31
5=> dec:53 | hex: 0x35
.=> dec:46 | hex: 0x2e
0=> dec:48 | hex: 0x30
4=> dec:52 | hex: 0x34
5=> dec:53 | hex: 0x35
3=> dec:51 | hex: 0x33
,=> dec:44 | hex: 0x2c
N=> dec:78 | hex: 0x4e
,=> dec:44 | hex: 0x2c
1=> dec:49 | hex: 0x31
0=> dec:48 | hex: 0x30
3=> dec:51 | hex: 0x33
4=> dec:52 | hex: 0x34
3=> dec:51 | hex: 0x33
.=> dec:46 | hex: 0x2e
7=> dec:55 | hex: 0x37
3=> dec:51 | hex: 0x33
1=> dec:49 | hex: 0x31
4=> dec:52 | hex: 0x34
,=> dec:44 | hex: 0x2c
W=> dec:87 | hex: 0x57
,=> dec:44 | hex: 0x2c
0=> dec:48 | hex: 0x30
.=> dec:46 | hex: 0x2e
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
,=> dec:44 | hex: 0x2c
4=> dec:52 | hex: 0x34
1=> dec:49 | hex: 0x31
,=> dec:44 | hex: 0x2c
1=> dec:49 | hex: 0x31
3=> dec:51 | hex: 0x33
1=> dec:49 | hex: 0x31
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
2=> dec:50 | hex: 0x32
,=> dec:44 | hex: 0x2c
,=> dec:44 | hex: 0x2c
*=> dec:42 | hex: 0x2a
2=> dec:50 | hex: 0x32
6=> dec:54 | hex: 0x36
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
.=> dec:46 | hex: 0x2e
8=> dec:56 | hex: 0x38
|=> dec:124 | hex: 0x7c
4=> dec:52 | hex: 0x34
8=> dec:56 | hex: 0x38
6=> dec:54 | hex: 0x36
|=> dec:124 | hex: 0x7c
2=> dec:50 | hex: 0x32
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
A=> dec:65 | hex: 0x41
,=> dec:44 | hex: 0x2c
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
8=> dec:56 | hex: 0x38
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
4=> dec:52 | hex: 0x34
E=> dec:69 | hex: 0x45
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
4=> dec:52 | hex: 0x34
0=> dec:48 | hex: 0x30
8=> dec:56 | hex: 0x38
4=> dec:52 | hex: 0x34
9=> dec:57 | hex: 0x39
4=> dec:52 | hex: 0x34
0=> dec:48 | hex: 0x30
A=> dec:65 | hex: 0x41
7=> dec:55 | hex: 0x37
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
C=> dec:67 | hex: 0x43
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
7=> dec:55 | hex: 0x37
F=> dec:70 | hex: 0x46
(=> dec:40 | hex: 0x28
�=> dec:178 | hex: 0xb2
=> dec:13 | hex: 0xd

=> dec:10 | hex: 0xa

OK DATA:242407e45221520187fff99553233323832352e3030302c412c313931352e303435332c4e2c31303334332e373331342c572c302e30302c34312c3133313031322c2c2a32367c302e387c3438367c323030307c303030412c303030387c303134453030313430383439343041377c30437c303030303030374628b2da

Line received: $$~E" ��U232825.000,A,1915.0453,N,10343.7314,W,0.00,41,131012,,*26|0.8|486|2000|000A,0008|014E0014084940A7|0C|0000007F(� [from IPv4Address(TCP, '189.254.205.154', 52392)]

Separado:

<$$=2424> <L=007E> <ID=45221520187fff> <CMD=9955> 3233323832352e3030302c412c313931352e303435332c4e2c31303334332e373331342c572c302e30302c34312c3133313031322c2c2a32367c302e387c3438367c323030307c303030412c303030387c303134453030313430383439343041377c30437c303030303030374628b2da


















Connection from IPv4Address(TCP, '200.95.171.23', 52693), 1 peers connected, 2 peers attended.
OK DATA:242401145221520187FFF5006E51DA
Line received: 242401145221520187FFF5006E51 [from IPv4Address(TCP, '200.95.171.23', 52693)]
Data lenght is not correct!   Expected:276 | Calculated:16


Connection from IPv4Address(TCP, '201.144.60.171', 22515), 1 peers connected, 3 peers attended.
OK DATA:242401145221520187FFF5006E51DA
Line received: 242401145221520187FFF5006E51 [from IPv4Address(TCP, '201.144.60.171', 22515)]
Data lenght is not correct!   Expected:276 | Calculated:16
Connection from IPv4Address(TCP, '200.95.171.21', 58834), 1 peers connected, 4 peers attended.
OK DATA:242401145221520187FFF5006E51DA
Line received: 242401145221520187FFF5006E51 [from IPv4Address(TCP, '200.95.171.21', 58834)]
Data lenght is not correct!   Expected:276 | Calculated:16
Connection from IPv4Address(TCP, '200.95.171.15', 30511), 1 peers connected, 5 peers attended.
OK DATA:242401145221520187FFF5006E51DA
Line received: 242401145221520187FFF5006E51 [from IPv4Address(TCP, '200.95.171.15', 30511)]
Data lenght is not correct!   Expected:276 | Calculated:16
Connection from IPv4Address(TCP, '200.95.171.147', 28581), 1 peers connected, 6 peers attended.
OK DATA:242407D45221520187FFF99553033353132332E3030302C412C313931352E303437342C4E2C31303334332E373338382C572C302E30302C302C3134313031322C2C2A31457C312E317C3531327C323030307C303030392C303030387C303134453030313430383439374139347C30447C3030303030304536664FDA
Line received: 242407D45221520187FFF99553033353132332E3030302C412C313931352E303437342C4E2C31303334332E373338382C572C302E30302C302C3134313031322C2C2A31457C312E317C3531327C323030307C303030392C303030387C303134453030313430383439374139347C30447C3030303030304536664F [from IPv4Address(TCP, '200.95.171.147', 28581)]
Data lenght is not correct!   Expected:2004 | Calculated:124
Connection from IPv4Address(TCP, '200.95.171.133', 28217), 1 peers connected, 7 peers attended.
OK DATA:242407D45221520187FFF99553033353132332E3030302C412C313931352E303437342C4E2C31303334332E373338382C572C302E30302C302C3134313031322C2C2A31457C312E317C3531327C323030307C303030392C303030387C303134453030313430383439374139347C30447C3030303030304536664FDA
Line received: 242407D45221520187FFF99553033353132332E3030302C412C313931352E303437342C4E2C31303334332E373338382C572C302E30302C302C3134313031322C2C2A31457C312E317C3531327C323030307C303030392C303030387C303134453030313430383439374139347C30447C3030303030304536664F [from IPv4Address(TCP, '200.95.171.133', 28217)]
Data lenght is not correct!   Expected:2004 | Calculated:124
Connection from IPv4Address(TCP, '200.95.171.13', 32895), 1 peers connected, 8 peers attended.
OK DATA:242407D45221520187FFF99553033353132332E3030302C412C313931352E303437342C4E2C31303334332E373338382C572C302E30302C302C3134313031322C2C2A31457C312E317C3531327C323030307C303030392C303030387C303134453030313430383439374139347C30447C3030303030304536664FDA
Line received: 242407D45221520187FFF99553033353132332E3030302C412C313931352E303437342C4E2C31303334332E373338382C572C302E30302C302C3134313031322C2C2A31457C312E317C3531327C323030307C303030392C303030387C303134453030313430383439374139347C30447C3030303030304536664F [from IPv4Address(TCP, '200.95.171.13', 32895)]
Data lenght is not correct!   Expected:2004 | Calculated:124





Connection from IPv4Address(TCP, '200.95.171.147', 58068), 1 peers connected, 1 peers attended.
$=> dec:36 | hex: 0x24
$=> dec:36 | hex: 0x24
=> dec:0 | hex: 0x0
}=> dec:125 | hex: 0x7d
E=> dec:69 | hex: 0x45
"=> dec:34 | hex: 0x22
=> dec:21 | hex: 0x15
 => dec:32 | hex: 0x20
=> dec:24 | hex: 0x18
=> dec:127 | hex: 0x7f
�=> dec:255 | hex: 0xff
�=> dec:153 | hex: 0x99
U=> dec:85 | hex: 0x55
0=> dec:48 | hex: 0x30
4=> dec:52 | hex: 0x34
1=> dec:49 | hex: 0x31
1=> dec:49 | hex: 0x31
3=> dec:51 | hex: 0x33
4=> dec:52 | hex: 0x34
.=> dec:46 | hex: 0x2e
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
,=> dec:44 | hex: 0x2c
A=> dec:65 | hex: 0x41
,=> dec:44 | hex: 0x2c
1=> dec:49 | hex: 0x31
9=> dec:57 | hex: 0x39
1=> dec:49 | hex: 0x31
5=> dec:53 | hex: 0x35
.=> dec:46 | hex: 0x2e
0=> dec:48 | hex: 0x30
5=> dec:53 | hex: 0x35
0=> dec:48 | hex: 0x30
7=> dec:55 | hex: 0x37
,=> dec:44 | hex: 0x2c
N=> dec:78 | hex: 0x4e
,=> dec:44 | hex: 0x2c
1=> dec:49 | hex: 0x31
0=> dec:48 | hex: 0x30
3=> dec:51 | hex: 0x33
4=> dec:52 | hex: 0x34
3=> dec:51 | hex: 0x33
.=> dec:46 | hex: 0x2e
7=> dec:55 | hex: 0x37
3=> dec:51 | hex: 0x33
7=> dec:55 | hex: 0x37
9=> dec:57 | hex: 0x39
,=> dec:44 | hex: 0x2c
W=> dec:87 | hex: 0x57
,=> dec:44 | hex: 0x2c
0=> dec:48 | hex: 0x30
.=> dec:46 | hex: 0x2e
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
,=> dec:44 | hex: 0x2c
0=> dec:48 | hex: 0x30
,=> dec:44 | hex: 0x2c
1=> dec:49 | hex: 0x31
4=> dec:52 | hex: 0x34
1=> dec:49 | hex: 0x31
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
2=> dec:50 | hex: 0x32
,=> dec:44 | hex: 0x2c
,=> dec:44 | hex: 0x2c
*=> dec:42 | hex: 0x2a
1=> dec:49 | hex: 0x31
0=> dec:48 | hex: 0x30
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
.=> dec:46 | hex: 0x2e
8=> dec:56 | hex: 0x38
|=> dec:124 | hex: 0x7c
5=> dec:53 | hex: 0x35
1=> dec:49 | hex: 0x31
7=> dec:55 | hex: 0x37
|=> dec:124 | hex: 0x7c
2=> dec:50 | hex: 0x32
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
8=> dec:56 | hex: 0x38
,=> dec:44 | hex: 0x2c
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
8=> dec:56 | hex: 0x38
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
4=> dec:52 | hex: 0x34
E=> dec:69 | hex: 0x45
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
4=> dec:52 | hex: 0x34
0=> dec:48 | hex: 0x30
8=> dec:56 | hex: 0x38
4=> dec:52 | hex: 0x34
9=> dec:57 | hex: 0x39
4=> dec:52 | hex: 0x34
0=> dec:48 | hex: 0x30
A=> dec:65 | hex: 0x41
7=> dec:55 | hex: 0x37
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
C=> dec:67 | hex: 0x43
|=> dec:124 | hex: 0x7c
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
0=> dec:48 | hex: 0x30
1=> dec:49 | hex: 0x31
2=> dec:50 | hex: 0x32
A=> dec:65 | hex: 0x41
=> dec:23 | hex: 0x17
�=> dec:253 | hex: 0xfd
=> dec:13 | hex: 0xd

=> dec:10 | hex: 0xa
OK DATA:2424007D45221520187FFF99553034313133342E3030302C412C313931352E303530372C4E2C31303334332E373337392C572C302E30302C302C3134313031322C2C2A31307C302E387C3531377C323030307C303030382C303030387C303134453030313430383439343041377C30437C303030303031324117FDDA
Line received: 2424007D45221520187FFF99553034313133342E3030302C412C313931352E303530372C4E2C31303334332E373337392C572C302E30302C302C3134313031322C2C2A31307C302E387C3531377C323030307C303030382C303030387C303134453030313430383439343041377C30437C303030303031324117FD [from IPv4Address(TCP, '200.95.171.147', 58068)]
Checking data lenght [007D]
handle position on-demand:: 45221520187
DATE: 2012-10-14 TIME: 04:11:34 | valid:True | LON:10343.7379 W LAT:1915.0507 N| Velocidad: (0.00 knots) 0.0 km/h | Heading: 0 
Finished. Closing connection to the peer 200.95.171.147 



















