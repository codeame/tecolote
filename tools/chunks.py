
def chunks(l, n):
    """ 
    Yield successive n-sized chunks from l.
    
    Source: http://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks-in-python
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def cleanIMEI(l):
    """
    Based on chunks code.
    We need to remove the '3' on digits only. This means we need to get the CHAR of the corresponding HEX ascii code.
    Since IMEI is only digits 0-9, then we dont need to convert other characters (spend more memory), 
    just discard leading '3' on every pair of digits.
    
    Miguel Chavez Gamboa
    """
    for i in xrange(0, len(l), 2):
        #we can discard first element.
        yield l[i+1:i+2]

def cleanFirmware(l):
    """
    Based on chunks code.
    We need to remove the '3' on digits only. This means we need to get the CHAR of the corresponding HEX ascii code.
    
    Miguel Chavez Gamboa
    """
    for i in xrange(0, len(l), 2):
        #print 'hex %s is string %s'%(l[i:i+2], chr(int(l[i:i+2],16)))
        yield chr(int(l[i:i+2], 16))


def cleanDecimal(l):
    '''
    Practically the same as cleanFirmware.
    It returns a string list representing the number in decimal string of the given number in hex.
    Miguel Chavez Gamboa
    '''
    for i in xrange(0, len(l), 2):
        yield chr(int(l[i:i+2], 16))


def hex2bin(n):
    '''
    It converts a hex number (str) into a binary represented as a string.
    '''
    minbits = 4 * len(n)
    try:
        r = bin(int(n,16))[2:].zfill(minbits)
    except:
        print 'ERROR on converting hex %s to binary string.'%n
        r = '0'.zfill(minbits)
    return r


def hex2ascii(l):
    """
    Converts a hex number represented as a string to its ascii characters.
    Examples:
        "24" => "$"
        "40" => "@"
    """
    for i in xrange(0, len(l), 2):
        original = l[i:i+2]
        new = chr(int(original, 16))
        #print 'Converting %s -> %s ==> %s'%(original, hex(int(original, 16)), new)
        yield new
