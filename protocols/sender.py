# -*- coding: utf-8 -*-


# Receiver Protocol - Sending commands
# Tecolote Server.
# A GPS Tracking Data-Collecting Server.
#
# (C) 2012  Miguel Chavez Gamboa.
# miguel@codea.me
#

import meiligao
#from meiligao.encoder import *

#TODO: For supporting more protocols, we need to import the proper modules, and decide which methods to use from them
#      And in order to do this, next methods needs to know the protocol to to use. 
# Example: send_cmd_getImei(self, sender, protocol) #the protocol is an object.
#          data = protocol.encoder.encode_cmd_getImei(...)

def send_cmd_headingChange(self, sender, angle):
    """
    Sends a cmd message for change the heading degree for position report.
    angle parameter is the angle in degrees, in string representing an integer.
    example: send_cmd_headingChange(self, '123456', '128')
    """
    new_data = meiligao.encoder.encode_cmd_headingChange(sender, angle)
    self.sendLine(new_data)

    # 41 41 00 12 52 21 52 01 87 FF F4 13 61 40 00 0
    # 41 41 00 13 52 21 52 01 87 FF F4 13 63 23 00 00 0

def send_cmd_getImei(self, sender):
    """
    Sends a cmd message for getting the IMEI, SN & Firmware Version of the device.
    Format: @@<L><ID><0x9001><checksum>\r\n
    """
    new_data = meiligao.encoder.encode_cmd_getImei(sender)
    self.sendLine(new_data)



def send_cmd_loginConfirmation(self, sender):
    """
    Sends the command LoginConfirmation to the device.
    Format is: @@<L><ID>4000<FLAG:00 for fail, 01 for success><CHECKSUM>
    For now, success. If we can reject login based on a white list or on states [e.g. Suspended], then we can send flag as 00.
    """
    new_data = meiligao.encoder.encode_cmd_loginConfirmation(sender)
    self.sendLine(new_data)


def send_cmd_heartbeat(self, sender, time):
    """
    Sends the heartbeat time interval.
    Format: @@<L><ID><0x5199><data><checksum> \r\n
    data=[0,65535], in ASCII code and in unit of minute
    """
    new_data = meiligao.encoder.encode_cmd_heartbeat(sender, time)
    self.sendLine(new_data)
    print 'Sent command [heartbeat]: %s'%new_data


def send_cmd_trackOnDemand(self, sender):
    """
    Sends the track on demand command.
    """
    new_data = meiligao.encoder.encode_cmd_trackOnDemand(sender)
    self.sendLine(new_data)
    print 'Sent command [trackOnDemand]: %s'%new_data

def send_cmd_powerDown(self, sender, time):
    """
    Sends the power down command for power saving.
    """
    new_data = meiligao.encoder.encode_cmd_powerDown(sender, time)
    self.sendLine(new_data)

def send_cmd_speedAlarm(self, sender, speed):
    """
    Sends the speed alarm command, the command sets the speed to fire the alarm.
    speed is the speed in km/h, in string hex, interger values only. speed=0 cancels the alarm.
    The alarm will be sent from the tracker each 30 seconds until the speed is lower
    than the speed set.
    """
    new_data = meiligao.encoder.encode_cmd_speedAlarm(sender, speed)
    self.sendLine(new_data)

def send_cmd_initialization(self, sender, time):
    """
    Sends the initialization command.
    """
    new_data = meiligao.encoder.encode_cmd_initialization(sender)
    self.sendLine(new_data)

def send_cmd_sleepMode(self, sender, level):
    """
    Sends the Sleep Mode command.
    """
    new_data = meiligao.encoder.encode_cmd_sleepMode(sender, level)
    self.sendLine(new_data)

def send_cmd_voiceMonitoringAuth(self, sender, phone):
    """
    Sends the Voice Monitoring Authoriztion Phone command for power saving.
    """
    new_data = meiligao.encoder.encode_cmd_voiceMonitoringAuth(sender, phone)
    self.sendLine(new_data)


def send_cmd_setTrembleSensorSensitivity(self, sender, sens):
    """
    Sends the Tremble Sensor command for setting the sensitivity.
    """
    new_data = meiligao.encoder.encode_cmd_setTrembleSensorSensitivity(sender, sens)
    self.sendLine(new_data)

def send_cmd_setGPSAntennaCut(self, sender, value):
    """
    Sends the GPS antenna cut alarm command. Only for VT400.
    """
    new_data = meiligao.encoder.encode_cmd_setGPSAntennaCut(sender, value)
    self.sendLine(new_data)


def send_cmd_setTrackByDistance(self, sender, val):
    """
    Sends the Track by Distance command.
    """
    new_data = meiligao.encoder.encode_cmd_setTrackByDistance(sender, val)
    self.sendLine(new_data)

def send_cmd_deleteMileage(self, sender):
    """
    Sends the Delete Mileage command.
    """
    new_data = meiligao.encoder.encode_cmd_deleteMileage(sender)
    self.sendLine(new_data)

def send_cmd_rebootGPS(self, sender):
    """
    Sends the Reboot GPS command. 
    WARNING: This command did not pass the test. It may be that the doc is wrong.
             Our generated checksum is not the same as the one in the doc example.
    """
    new_data = meiligao.encoder.encode_cmd_rebootGPS(sender)
    self.sendLine(new_data)

