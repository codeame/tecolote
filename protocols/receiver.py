# -*- coding: utf-8 -*-


# Receiver Protocol
# Tecolote Server.
# A GPS Tracking Data-Collecting Server.
#
# (C) 2012  Miguel Chavez Gamboa.
# miguel@codea.me
#

from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver

#CRC and binascii
import crcmod
import binascii

import psycopg2
from twisted.enterprise import adbapi 
from twistar.registry import Registry
from twistar.dbobject import DBObject

import datetime
import decimal
from decimal import Decimal

#Logs: everything written to stdout (eg print) and stderr will be logged.
from twisted.python import log
from twisted.python.logfile import DailyLogFile

# My tools
from tools.chunks import cleanIMEI, cleanFirmware, cleanDecimal, hex2bin
from protocols.sender import *
from protocols.meiligao.decoder import *
from protocols.meiligao.encoder import *


class Login(DBObject):
    TABLENAME = "login"

class Position(DBObject):
    TABLENAME = "positions"

class Alarm(DBObject):
    TABLENAME = "alarms"

class TrackerReceiver(LineReceiver):
    """
    MEILIGAO Protocol.
    The tracker (MEILIGAO) protocol, parsed according to the VT310 compatible devices.
    The VT310 is our testing device.

    Commands now accepted are:
        LOGIN
        IMEI & SN response -- $$<L><ID><0x9001><data><checksum>\r\n
        HEARTBEAT       -- 0x00  @@<L><ID><0x5199><data><checksum> \r\n / $$<L><ID><0x5199><Flag><checksum> \r\n
        ALARM           --  $$<L><ID><0x9999><Alarm><data><checksum>\r\n  <Alarm> is one byte in hex that represents the alarm fired. See manual.
        POSITION_ONDEMAND  --  $$<L><ID><0x9955><data><checksum>\r\n (at least in response on 4101 cmd [track-on-demand])
        HEADING CHANGE  --  @@<L><ID><0x4136><data><checksum>\r\n / $$<L><ID><0x4136><Flag><checksum> 

    All data received in the line is a string representing hex data without the 0x chars. (0x0d => 0d)
    First two bytes are the starting string '$$' when comming from the device.
    Next two bytes are for the data lenght.
    Next 7 bytes are for device ID (presumibly the IMEI). Not used digits are filled with an F, and need to be removed.
    Next 2 bytes are for the command
    Next 0 to 100 bytes are for the data when it applies.
    Next 2 bytes are for the checksum which happens to be a CRC-CCITT with initCrc=0xFFFF.
    The ending delimiter is '0D0A' which is removed by the lineReceiver protocol. But we must take it into account for the length of the package.

    NOTES:

        Some commands will cause response from the device, so we must take that into account for doing wathever is needed (ie respond).
        And to wait until we complete the communication, to finally close the connection.

        Example is the Track On demand (0x4101) command. This command will cause the device to reply with its current location.
        It will send back the command 0x9955 wth the data correspondig to the location (POSITION_ONDEMAND)

        * It seems that the <ID> sent on each command is not the IMEI it is the Serial number of the device. We can get the IMEI sending 
        a command to the device (90001: get IMEI & SN), imediatly after login but before closing the connection.

    """

    #delimiter = '0D0A'
    # it comes in hex. This is necesary to declare for the lineReceived protocol to remove it for us.
    # One problem is that if defined this way, the delimiter is not detected when making manual TESTS (pressing ENTER key) [debugging]


    # TODO: Add reply for commands sent by the tracker. Example: heading change (4136).
    commands = {
        # Commands Implemented
        # First Commands to be sent by the device by its own.
        '5000': 'LOGIN',
        '9001': 'IMEI_RESPONSE',
        '9955': 'POSITION',
        '5199': 'HEARTBEAT',
        '9999': 'ALARM',
        #Reply to sent commands
        #'4136': 'HEADING_CHANGE_SET',
        #'4105': 'SPEED_ALARM_SET',
        #'4110': 'INITIALIZATION_SET',
        #'4113': 'SLEEP_MODE_SET',
        #'4114': 'OUTPUT_CONTROL_CONDITIONAL_SET',
        #'4115': 'OUTPUT_CONTROL_SET',
    }


    def dataReceived(self, data):
        """
        Reimplemented for converting the delimiter from 0d0a to 0D0A, since we dont know exactly how it comes from the device.
        The data comes and the twisted/python converts to string the bytes arrived. This makes things worse, so we need the bytes back.
        At least in the initial phase to know how things really come from the device (example, 0-9 is one char, but we expected to be two 00,01.)
        Also note that delimiter comes as DA (a single byte) and not (0d0a) as we expected (two bytes, as stated by the docs).

        Miguel Chavez Gamboa.
        """
        ndata = ''
        if data.startswith("2424"):
            data = data.upper().replace('0D0A', '\r\n')
            LineReceiver.dataReceived(self, data)
        else:
            for d in data:
                d_ord = ord(d)
                d_hex = hex(d_ord)
                #print d +'=> dec:%s | hex: %s'%(d_ord, d_hex)
                if d_ord < 16: 
                    c = '0%s'%d_hex.replace('0x', '').upper()
                else:
                    c = '%s'%d_hex.replace('0x', '').upper()
                ndata = ndata + c
            print 'OK DATA [%s bytes]:\n'%len(data) +ndata
            LineReceiver.dataReceived(self, ndata.replace('0D0A', '\r\n'))

    def connectionMade(self):
        self.factory.count += 1 #Total count of peers attended so far.
        self.factory.peers += 1 #Total count of peers connected now.
        print '|%s|Connection from %s, %s peers connected, %s peers attended.' % (datetime.datetime.now(),self.transport.getPeer().host, self.factory.peers, self.factory.count)
        #send_cmd_headingChange(self, '45221520187', '10') #IT WORKS!
        #send_cmd_getImei(self, '45221520187')  #IT WORKS!
        #send_cmd_heartbeat(self, '45221520187', '60') # IT WORKS!
        #send_cmd_trackOnDemand(self, '45221520187')
        #send_cmd_powerDown(self, '45221520187', '5') #IT WORKS!

        #NOTE: It seems to be that only one message per connection is attended. If we send two, only the first is replied.
        #      Maybe, this is because the messages are sent together in the same tcp package [viewd on wireshark]:
        # Here are 3 commands [9001, 5199,4101] that were sent in one package, 3 lines of 'text': 
        # 4040001145221520187FFF9001A405\r\n4040001245221520187FFF5199312E63\r\n4040001145221520187FFF41018213
        # The tracker only reply to the first one.
        #
        # The question is how to force the LineReceiver Protocol to send each line in a different tcp package.
        # *due to tcp package max lenght?




    def connectionLost(self, reason):
        self.factory.peers -= 1


    def lineReceived(self, line):
        parsed = parse_command(line)

        if not parsed['valid']:
            print 'PARSE ERROR: %s'%parsed['error']
            self.transport.loseConnection()
            return

        #Check for the data lenght
        if parsed['data_len'] != (len(line+'0d0a')/2):
            print 'Data lenght is not correct!   Expected:%s | Calculated:%s'%(parsed['data_len'], len(line+'ODOA')/2)
            self.transport.loseConnection()
            return

        try:
            # Using package:  crcmod  (http://crcmod.sourceforge.net/crcmod.predefined.html)
            crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
            our_checksum = hex(crc16ccitt(binascii.a2b_hex(line[:-4])))
        except (ValueError, TypeError):
            print 'Error on value in the checksum.'
            self.transport.loseConnection()
            return

        if parsed['checksum'] != our_checksum:
            print 'CRC Error. Cannot continue...'
            self.transport.loseConnection()
            return

        if not parsed['cmd'] in self.commands:
            print 'Not recognized command (%s). LINE is: \n %s'%(parsed['cmd'], line)
            self.transport.loseConnection()
            return

        #route to the proper handler
        handler = getattr(self, "handle_%s" % self.commands[parsed['cmd']], None)
        if handler:
                handler(parsed['senderID'], parsed['data'])
        else:
            print 'No handler available for command %s'%parsed['cmd']

        #finally close the connection.
        # loseConnection() allows to process any data already send by the client and waits until everything is attended for closing the connection.
        print 'Finished. Closing connection to the peer %s \n'%self.transport.getPeer().host
        self.transport.loseConnection()


    def handle_LOGIN(self, sender, data):
        """
        Handles the login call from the device.
        It responds to the device with a login_confirmation.
        sender is the IMEI, data is empty.

        Example data:
        24240011123456FFFFFFFF50008B9B0D0A

        The response seems to be sent using the incomming connection and not stablishing another.
        Unfortunately the device's protocol documentation is poor.
        """
        def cbLogin(d):
                print 'LOGIN saved to database: %s from %s @ %s '%(d.device_id, d.ip, d.date_time)

        print 'HANDLE LOGIN :: Sender: %s | From: %s | @ %s'%(sender, self.transport.getPeer().host, datetime.datetime.now())

        #First record the login attempt at the database.
        Registry.DBPOOL = adbapi.ConnectionPool('psycopg2', user="tecolote", password="xarwit0721", database="tecolotedb")
        l = Login()
        l.device_id = sender
        l.ip = self.transport.getPeer().host
        l.date_time = datetime.datetime.utcnow() #Saving in utc.
        try:
            l.save().addCallback(cbLogin)
        except:
            print 'Error saving login to database.'
        Registry.DBPOOL.close() # This to free the connection pool. UNDOCUMENTED on twistar|twisted.

        #Then respond to the client with a login_confirmation
        send_cmd_loginConfirmation(self, sender)

        #Now send the 9001 command (GET IMEI & SN)
        send_cmd_getImei(self, sender)

        #we can finish connection, we dont need to send more commands.
        self.transport.loseConnection()


    def handle_IMEI_RESPONSE(self, sender, data):
        """
        This handles the GET_IMEI_&_SN response from the device. Format is:
        $$<L><ID><0x9001><data><checksum>\r\n

        The data format is, represented in HEX, as follows: IMEI,SN,FIRMWARE_VERSION

        * IMEI:
            The number (only digits 0-9), represented in HEX Strings. Numbers from 0 to 9 in hex are 3N where N is the number. Example: 5 -> 0x35 (0x35 = '5').
        * SN:
            The number (only digits 0-9), represented in HEX Strings. Numbers from 0 to 9 in hex are 3N where N is the number. Example: 5 -> 0x35 (0x35 = '5').
        * FIRMWARE_VERSION:
            The firmware version is a string (digits and chars) in HEX. Examples: 56='V', 2D='-', 4E='N', 38='8'
        * The DELIMITER between each field is a colon (',') which is 2C in HEX.

        A clue about the data format was found on https://sourceforge.net/projects/opengts/forums/forum/579835/topic/4793926
        Anything else was simply deduced from HEX in the example.
        """
        data_list = data.split('2C')
        sn = data_list[0]
        imei = data_list[1]
        fv = data_list[2]

        #Using methods from tools module.
        #cleaning IMEI
        imei = ''.join(list(cleanIMEI(imei)))
        #clean SN
        sn = ''.join(list(cleanIMEI(sn)))
        #clean sv
        fv = ''.join(list(cleanFirmware(fv)))
        print 'HANDLE IMEI RESPONSE  -- SenderID: %s | IMEI: %s | SERIAL NUMBER: %s | FIRMWARE VERSION: %s'%(sender, imei,sn,fv)

        #Now we can save the data.
        print 'TODO: Save to database. We need to see if it already exists to update or insert a new.'

        self.transport.loseConnection()


    def handle_POSITION(self, sender, data):
        """
        
        Handles the position report from the device. 
        This command is sent in response of various commands sent to the device (track-on-demand, track-by-interval...)

        It validates data and saves to the database (send to the task queue?).

        Data consists of: GPRMC | HDOP | Altitude | State | AD| BASE ID | CSQ | Journey
        Where | is the separator and in hex is 0x7C

        GPRMC includes: hhmmss.dd,S,xxmm.dddd,<N|S>,yyymm.dddd,<E|W>,s.s,h.h,ddmmyy,d.d,D*HH

        For example GPRMC: 134829.486,A,2232.6083,N,11404.8137,E, 58.31,309.62,010809,12.1,112,230809,,*1A

        """
        def cb_position(d):
            print 'POSITION saved to database: %s'%(d)
        def cb_posError(failure):
            print '|MCH| ERROR SAVING POSITION TO DB : %s'%failure.getErrorMessage()
            #failure.trap(psycopg2.IntegrityError, psycopg.IntegrityError)
            return None

        print 'HANDLE POSITION  :: %s -- at %s'%(sender, datetime.datetime.utcnow())
        #Any black/white list for filtering senders? a future feature.

        #we can finish connection, we dont need to send more commands.
        self.transport.loseConnection()

        info = decode_data(data) #returns a dict

        Registry.DBPOOL = adbapi.ConnectionPool('psycopg2', user="tecolote", password="xarwit0721", database="tecolotedb")
        p = Position()
        p.device_id = sender
        p.date_time = datetime.datetime.combine(info['gprmc_pdate'], info['gprmc_ptime'])
        #date-time from device is UTC, depending on daylight-saving-time and the zone, is 5 or more hours ahead
        p.server_date_time = datetime.datetime.utcnow() #a few milliseconds/seconds after the data arrived, due to processing time.
        p.posicion  = "SRID=4326;POINT(%s %s)"%(info['gprmc_lon'], info['gprmc_lat'])
        p.valid = info['gprmc_valid']
        p.speed = Decimal(info['gprmc_speed_km'])
        p.journey = Decimal(info['journey'])
        p.heading = info['gprmc_heading']
        p.hdop = info['hdop']
        p.altitude = info['altitude']
        p.ad = info['ad']
        p.state = info['state']
        p.base_id = info['base_id']
        p.csq = info['csq']
        p.journey = info['journey']
        p.save().addCallbacks(cb_position, cb_posError)
        Registry.DBPOOL.close() # This is to free the connection pool. UNDOCUMENTED on twistar|twisted. If not called, the connections keep idle until the server stops, and postgres stop receiving connections.

        print 'DATETIME: %s'%p.date_time.isoformat(' ')

    def handle_ALARM(self, sender, data):
        """
        ALARM --  $$<L><ID><0x9999><Alarm><data><checksum>\r\n  
        <Alarm> is one byte in hex that represents the alarm fired:
            =0x01 SOS button is pressed / Input 1 active
            =0x02 Call B button is pressed / Input 2 active
            =0x03 Call C button is pressed / Input 3 active
            =0x04 Input 4 active
            =0x05 Input 5 active
            =0x10 Low battery alarm
            =0x11 Speeding alarm
            =0x12 Movement alarm or alarm of tracker exiting Geo-fence scope
            =0x13 Alarm of tracker entering Geo-fence scope
            =0x14 Alarm of tracker being turned on
            =0x15 Alarm of tracker entering GPS blind area
            =0x16 Alarm of tracker exiting GPS blind area
            =0x31 SOS button is released/Input 1 inactive
            =0x32 Call B button is released/Input 2 inactive
            =0x33 Call C button is released/Input 3 inactive
            =0x34 Input 4 inactive
            =0x35 Input 5 inactive
            =0x50 External power cut alarm
            =0x52 Veer report   |MCH| Esta alarma es en respuesta del comand heading-change (4136)
            =0x53 GPS antenna cut alarm
            =0x63 Distance report

        <data> is the info with gprmc and the other info; the same as in tracking position report.
        """
        def cb_position(d):
            print 'ALARM SAVED: %s'%d

        def cb_posError(failure):
            print 'ERROR SAVING ALARM TO DB : %s'%failure.getErrorMessage()
            return None

        def cb_alarm_posError(failure):
            print 'ERROR SAVING ALARM POSITION TO DB : %s'%failure.getErrorMessage()
            return None

        def cb_alarm_pos(d):
            print 'ALARM_POSITION saved to database: %s'%(d)
            Registry.DBPOOL = adbapi.ConnectionPool('psycopg2', user="tecolote", password="xarwit0721", database="tecolotedb")
            #Now save the ALARM DATA.
            a = Alarm()
            a.device_id = d.device_id
            a.alarm_type = '%s'%d._alarm
            a.posicion_id  = d.id
            a.save().addCallbacks(cb_position, cb_posError)
            Registry.DBPOOL.close()

        #we can finish connection, we dont need to send more commands.
        self.transport.loseConnection()

        alarm = data[:2]
        data = data[2:]
        print 'HANDLE ALARM - Type: %s'%alarm
        info = decode_data(data)


        # SOMETHING TO CONSIDER:
        # Basically the alarm says that some alarm is fired and the POSITION the device was at the moment.
        # So we can know where the car was at the moment the alarm was fired.
        # For example, if the SOS button was pressed, it can be known where the car was when the SOS was activated.


        Registry.DBPOOL = adbapi.ConnectionPool('psycopg2', user="tecolote", password="xarwit0721", database="tecolotedb")

        #First save the POSITION DATA
        p = Position()
        p.device_id = sender
        p.date_time = datetime.datetime.combine(info['gprmc_pdate'], info['gprmc_ptime'])
        #date-time from device is UTC, depending on daylight-saving-time and the zone, is 5 or more hours ahead
        p.server_date_time = datetime.datetime.utcnow() #a few milliseconds/seconds after the data arrived, due to processing time.
        p.posicion  = "SRID=4326;POINT(%s %s)"%(info['gprmc_lon'], info['gprmc_lat'])
        p.valid = info['gprmc_valid']
        p.speed = Decimal(info['gprmc_speed_km'])
        p.journey = Decimal(info['journey'])
        p.heading = info['gprmc_heading']
        p.hdop = info['hdop']
        p.altitude = info['altitude']
        p.ad = info['ad']
        p.state = info['state']
        p.base_id = info['base_id']
        p.csq = info['csq']
        p.journey = info['journey']
        #just a trick
        p._alarm = '%s'%alarm
        p.save().addCallbacks(cb_alarm_pos, cb_alarm_posError)
        
        Registry.DBPOOL.close() # This is to free the connection pool. UNDOCUMENTED on twistar|twisted. If not called, the connections keep idle until the server stops, and postgres stop receiving connections.

        # The looseConnection is called at the lineReceived method.

    
    def handle_HEARTBEAT(self, sender, data):
        """
        Receives the heartbeat data from the device.

        It seems that the device sends a command [5199] with data=0x01 (doc states data=0x00). Also it seems that
        the package is divided into two sequences, the first, tcp.data=00, the second contains the command with data=0x01
        [Analizing with wireshark]

        """

        print 'HANDLE HEARTBEAT -- Sender: %s | Data: %s'%(sender,data)

        #we can finish connection, we dont need to send more commands.
        self.transport.loseConnection()




class TrackerReceiverFactory(Factory):
    protocol = TrackerReceiver

    def __init__(self):
        """
        """
        self.count = 0 #Number of peers attended
        self.peers = 0 #Number of online peers
        log.startLogging(DailyLogFile.fromFullPath("server.log"))
        log.msg('STARTING TECOLOTE SERVER!')
