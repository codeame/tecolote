# -*- coding: utf-8 -*-
#
# Tecolote Server.
# A GPS Tracking Data-Collecting Server.
#
# Decoder tools for the MEILIGAO Protocol.
#
# The VT310 is our testing device.
#
# (C) 2012  Miguel Chavez Gamboa.
# miguel@codea.me

import crcmod
import binascii

import datetime
import decimal
from decimal import Decimal

#My tools
from tools.chunks import cleanIMEI, cleanFirmware, cleanDecimal, hex2bin


def parse_command(line):
    """
    Parses the line received.
    Returns a dict with the different fields on the command line, and a check field for validate the line.
    If invalid command line, it also reports the reason (the first one to appear).
    """
    r = {}
    r['valid'] = True

    if not line.startswith('2424'):
        r['valid'] = False
        r['error'] = "Invalid data. Missing starting sequence: $$"
        return r #Finish early.

    r['cmd'] = line[22:26]
    r['data'] = line[26:-4]
    #cleaning the senderID, represented in DIGITS [0..9] 7 bytes or 14 digits, unused bytes are filled with F values.
    r['senderID'] = line[8:22].upper().replace('F', '')

    try:
        r['data_len'] = int(line[4:8], 16)
        r['checksum'] = hex(int(line[-4:], 16))
    except (ValueError, TypeError):
        r['valid'] = False
        r['error'] = 'Error on value, expected a number on data length or checksum [%s / %s].'%(line[4:8] ,line[-4:])
        return r


    return r


def decode_data(data):
    """
    Decodes the data information on some reports from the device.
    The data is GPRMC | HDOP | Altitude | State | AD| BASE ID | CSQ | Journey.
    It does not include the sender, the command length, the checksum nor the delimiter.
    It returns a dict with all the decoded data.
    """
    info = {}
    params = data.split('7C') #'7C=|')

    #Testing...
    # cuenta = 0
    # print '\n---------------- Iterating Parameters ----------------'
    # for p in params:
    #     print 'Param %s: [%s]'%(cuenta, p)
    #     cuenta += 1
    #     if '2C' in p:
    #         print 'Found a [,] in parameters, splitting...'
    #         for sp in p.split('2C'):
    #             print '     -> %s'%sp
    # print '\n------------------------ Ok --------------------------'

    #Get the GPRMC data set
    gprmc = params[0].split('2C') # delimier is ',' -> 0x2C
    gprmc_time = gprmc[0] # format : hhmmss.ddd in hex (HHHHMMMMSSSS.DDDDDD)  '.' -> 0x2E
    gprmc_ptime = datetime.time(
        int(chr(int(gprmc_time[0:2],16))+chr(int(gprmc_time[2:4],16))), # HOUR
        int(chr(int(gprmc_time[4:6],16))+chr(int(gprmc_time[6:8],16))), #MINUTE
        int(chr(int(gprmc_time[8:10],16))+chr(int(gprmc_time[10:12],16))), #SECOND
        int(chr(int(gprmc_time[14:16],16))+chr(int(gprmc_time[16:18],16))+chr(int(gprmc_time[18:20],16))) #MICROSECONDS
    )
    gprmc_valid = True if gprmc[1] == '41' else False # 'A' -> 0x41
    info['gprmc_ptime'] = gprmc_ptime
    info['gprmc_valid'] = gprmc_valid
    # lon and lat are formated as: xxmm.dddd, where xx is degrees, mm is minutes, and dddd is miliseconds [MinDec format]
    # So, 11404.8137 means 114 degrees, 04 minutes, 8137 miliseconds (or 114 degrees, 4.8137 minutes)
    # converting from MinDec to Decimal Degree is:  DecimalDegrees/60 + Degrees. 
    # Ex: 114° 4.8137 => 114 + 4.8137/60 = 114.080228333
    # Minutes are 0 t0 59 (2 digits max), Degrees are -180 t0 180 (3 digits max)
    gprmc_lat = gprmc[2] #in format ABCD.EFGH '.' -> 0x2E
    gprmc_lat = ''.join(list(cleanDecimal(gprmc_lat))) #In string.
    gprmc_latNS = ''.join(list(cleanDecimal(gprmc[3])))
    gprmc_lon = gprmc[4] #in format ABCD.EFGH '.' -> 0x2E
    gprmc_lon = ''.join(list(cleanDecimal(gprmc_lon))) #In string.
    gprmc_lonEW = ''.join(list(cleanDecimal(gprmc[5])))
    gprmc_speed_knots = gprmc[6]
    gprmc_speed_knots = ''.join(list(cleanDecimal(gprmc_speed_knots))) # In string.
    gprmc_speed_km = float(gprmc_speed_knots) * float(1.852) # In float.
    gprmc_heading = ''.join(list(cleanDecimal(gprmc[7])))
    gprmc_heading = float(gprmc_heading)
    gprmc_date = gprmc[8]
    gprmc_pdate = datetime.date(
        int(chr(int(gprmc_date[8:10],16))+chr(int(gprmc_date[10:12],16))) + 2000, # YEAR It comes in 2 digits (2009 -> 09)
        int(chr(int(gprmc_date[4:6],16))+chr(int(gprmc_date[6:8],16))), # MONTH
        int(chr(int(gprmc_date[0:2],16))+chr(int(gprmc_date[2:4],16))) # DAY
    )

    info['gprmc_speed_km'] = gprmc_speed_km
    info['gprmc_speed_knots'] = gprmc_speed_knots
    info['gprmc_heading'] = gprmc_heading
    info['gprmc_pdate'] = gprmc_pdate

    #converting lon and lat to floats with its corresponding sign.
    #Here I suppose that minutes are <10 are written with a leading zero (ie, 9 = '09' )
    degmin,sec = gprmc_lat.split('.')
    minutes = degmin[-2:]
    degmin = degmin.replace(minutes, '', 1) #replace only the first occurrence.

    try:
        degmin = float(degmin)
        minutes = float(minutes)
        sec = float('0.%s'%sec)
        gprmc_lat = degmin + (minutes+sec)/60 
    except:
        print '(decoder @ 129) ERROR when converting some of this values> degmin:%s | minutes:%s | sec: %s'%(degmin, minutes, sec)
        #here iw will continue failing. some values may contain unknown data if this happens.
    
    if gprmc_latNS == 'S':
        gprmc_lat = -gprmc_lat

    degmin,sec = gprmc_lon.split('.')
    minutes = degmin[-2:]
    degmin = degmin.replace(minutes, '', 1) #replace only the first occurrence.
    try:
        degmin = float(degmin)
        minutes = float(minutes)
        sec = float('0.%s'%sec)
        gprmc_lon = degmin + (minutes+sec)/60 
    except:
        print '(decoder @ 143) ERROR when converting some of this values> degmin:%s | minutes:%s | sec: %s'%(degmin, minutes, sec)

    if gprmc_lonEW == 'W':
        gprmc_lon = -gprmc_lon

    #print 'LAT: %s  LON: %s'%(gprmc_lat, gprmc_lon)
    info['gprmc_lat'] = gprmc_lat
    info['gprmc_lon'] = gprmc_lon

    # magnetic variation is ignored (empty) with its heading. Here it seems it comes in a single field.
    # an asterisk (0x2A) followed by the checksum.
    gprmc_checksum = gprmc[10].replace('2A', '') #already in hex string. Only removing the Asterisk (0x2A)
    info['gprmc_checksum'] = gprmc_checksum
    #NOTE: We do not check the grpmc checksum. 

    hdop = ''.join(list(cleanDecimal(params[1])))
    try:
        hdop = float(hdop)
    except:
        print 'HDOP conversion failed, value was "%s" '%hdop
        hdop = float(0.0)

    # HDOP is the accuracy for lon and lat. gps-babbel has a filter-discard, a sample sets to 10 the max hdop value accepted.
    # Also in wikipedia: http://en.wikipedia.org/wiki/Dilution_of_precision_(GPS)
    if hdop > 12:
       print 'WARNING :: HDOP (%s) is high'%(hdop)
    if hdop < 0.5:
        print 'WARNING :: HDOP (%s) is LOW.'%hdop

    altitude = ''.join(list(cleanDecimal(params[2])))
    altitude = float(altitude)

    info['hdop'] = hdop
    info['altitude'] = altitude

    #State of inputs and outputs.
    #NOTE: Here there are differencies between devices.
    #We would need a way to detect what is the device type in order to parse inputs/outpus correctly.
    # HOW TO IDENTIFY THE DEVICE MODEL?
    #  -> One solution would be that the customer specify this information when registering the device on the platform.


    len_params = len(params)
    if len_params > 3:
        state = params[3]
    else:
        state = ''
        print 'NO STATE parameter.'
    if len_params > 4:
        ad = params[4]
    else:
        ad = ''
        print 'NO AD Parameter.'
    if len_params > 5:
        base_id = params[5]
    else:
        base_id = ''
        print 'No BASE ID parameter.'
    if len_params > 6:
        csq = params[6]
    else:
        csq = ''
        print 'No CSQ parameter.'
    if len_params > 7:
        #Journey is an INT, in HEX, in meters.
        journey = ''.join(list(cleanDecimal(params[7])))
        journey = journey.lstrip('0')
        try:
            journey = int(journey, base=16)
        except:
            journey = 0
            print 'Error converting Journey: %s'%journey
    else:
        journey = ''
        print 'No JOURNEY parameter.'


    # Sate comes in HEX, each hex digits has 4 bits.
    # State is 16 bits on VT300/VT310/VT400. 4 Hex digit.
    #    On VT300: Bit 0 and Bit 8 are used, the others are RESERVED (not used).
    #              Bit 0 is the Status of Output 1. Bit0=0: Out1 is closed; Bit0=1:Out1 is open.
    #              Bit 8 is the Status of Input 1.  Bit8=0: Input1 is invalid; Bit8=1: Input1 is valid (be pressed/connected to negative).
    #    On VT310:
    #              Bit 0  Status of Output 1 [1=open, 0=closed]
    #              Bit 1  Status of Output 2 [1=open, 0=closed]
    #              Bit 2  Status of Output 3 [1=open, 0=closed]
    #              Bit 3  Status of Output 4 [1=open, 0=closed]
    #              Bit 4  Status of Output 5 [1=open, 0=closed]
    #              Bit 5~7 are reserved.
    #              Bit 8  Status of Input  1 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 9  Status of Input  2 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 10 Status of Input  3 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 11 Status of Input  4 [0=Invalid, 1=Valid (be pressed/connected to positive) ] *Positive triggered
    #              Bit 12 Status of Input  5 [0=Invalid, 1=Valid (be pressed/connected to positive) ] *Positive triggered
    #              Bit 13~15 are reserved and defaulted as 0.
    #
    #0010 0000 0000 0000
    #
    #   On VT400:
    #              Bit 0  Status of Output 1 [1=open, 0=closed]
    #              Bit 1  Status of Output 2 [1=open, 0=closed]
    #              Bit 2~7 are reserved.
    #              Bit 8  Status of Input  1 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 9  Status of Input  2 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 10~15 reserved and defaulted as 0.
    #
    # State is 10 on GT30i/GT60. 
    #   On GT30i/GT60:
    #              Bit 0~7 reserved and defaulted as 0.
    #              Bit 8  Status of Input 1 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 9  Status of Input 2 [0=Invalid, 1=Valid (be pressed/connected to negative) ]
    #              Bit 10 Status of Input 3 [0=Invalid, 1=Valid (be pressed/connected to negative) ]

    #Decimalize some paramenters
    state = ''.join(list(cleanDecimal(state)))
    ad = ''.join(list(cleanDecimal(ad))) #contains a comma.
    #print 'STATE DECIMALIZED: %s | AD DECIMALIZED: %s'%(state, ad)

    #convert to binary.
    state = hex2bin(state)
    ad_l = []
    for v in ad.split(','):
        ad_l.append(hex2bin(v))
    ad = ','.join(ad_l)
    #print 'BIN STATE: %s | BIN AD: %s'%(state,ad)


    #if len(state) > 15:
        #VT300/VT310/VT400
    #else:
        #GT30i/GT60


    # AD: analog input (default voltage input) in HEX string.
    # For VT310:
    #   AD1, AD2: 10 bits, 0x0000~0x03ff in HEX, separated by ','
    #
    # For VT400:
    #   AD1..AD8: 12 bits, 0x0000~0x0fff in HEX, separated by ','
    #   * Note: AD1 is the value of external power.


    info['state'] = state
    info['ad'] = ad
    info['base_id'] = base_id
    info['csq'] = csq
    info['journey'] = journey

    #print 'Time STR: %s'%(''.join(list(cleanDecimal(gprmc_time))))
    print 'DATE: %s TIME: %s | valid:%s | LON:%s %s LAT:%s %s| Velocidad: (%s knots) %s km/h | Heading: %s | HDOP: %s | Altitude: %s | State: %s | AD: %s | Base ID: %s | CSQ: %s | Journey: %s' % \
    (gprmc_pdate, gprmc_ptime, gprmc_valid,gprmc_lon, gprmc_lonEW, gprmc_lat, gprmc_latNS, gprmc_speed_knots, gprmc_speed_km, gprmc_heading, hdop, altitude, state, ad, base_id, csq, journey)

    return info

