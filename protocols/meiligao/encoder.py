# -*- coding: utf-8 -*-
#
# Tecolote Server.
# A GPS Tracking Data-Collecting Server.
#
# Encoder tools for the MEILIGAO Protocol.
#
# The VT310 is our testing device.
#
# (C) 2012  Miguel Chavez Gamboa.
# miguel@codea.me

import crcmod
import binascii
from tools.chunks import hex2ascii


def encode_cmd_headingChange(sender, angle):
    """
    Encondes the headingChange Command, for change the heading degree for position report.
    angle parameter is the angle in degrees, in string representing an integer.
    Returns the encoded command in hex string.
    """
    strAngle = ''
    for char in angle:
        strAngle += '3%s'%char
    angle = strAngle
    while len(sender) < 14:
            sender = sender+'F'
    new_data = '40400000%s4136%s' % (sender, angle)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data # @ = 0x40
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data



def encode_cmd_getImei(sender):
    """
    Encodes the getImei Command, for asking the tracker for imei, sn, and firmware version.
    Returns the encoded command in hex string.
    """
    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s9001' % (sender)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data



def encode_cmd_loginConfirmation(sender):
    """
    Encodes the login confirmation command, for sending to the tracker the login confirmation.
    For now, success. If we can reject login based on a white list or on states [e.g. Suspended], then we can send flag as 00.
    Returns the command in hex string.
    """
    flag = '01' 
    while len(sender) < 14:
        sender = sender+'F'
    new_data = '40400000%s4000%s' % (sender, flag)  #stills missing the checksum and new lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    
    #now that we have the new_len, we replace the old length with it.
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except ValueError as e:
        print 'Error on value for the new checksum. Message will have an invalid CRC! \nError: %s'%e.strerror
        new_checksum = '0000'
    except TypeError as e:
        print 'Error on type for the new checksum. Message will have an invalid CRC! \nError: %s'%e.strerror
        new_checksum = '0000'
    except:
        print 'Error getting the new checksum. Message will have an invalid CRC! \nUnexpected error!'
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_heartbeat(sender, time):
    """
    Encondes the heartbeat Command, for change the time lapse for the heartbeat.
    time is the time in minutes, in string [values from 0 to 65535].
    Returns the encoded command in hex string.
    example: encode_cmd_heaeartbeat('123456', '12') # sender='123456'; time='12' minutes
    """
    strTime = ''
    for char in time:
        strTime += '3%s'%char
    time = strTime
    while len(sender) < 14:
            sender = sender+'F'
    new_data = '40400000%s5199%s' % (sender, time)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data # @ = 0x40
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_trackOnDemand(sender):
    """
    Encodes the Track On Demand Command, for asking the tracker for current position.
    Format: @@<L><ID><0x4101><checksum>\r\n
    """
    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4101' % (sender)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'TrackOnDemand Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data

def encode_cmd_speedAlarm(sender, speed):
    """
    Encondes the speed alarm command, the command sets the speed to fire the alarm.
    speed is the speed in km/h, in string hex, interger values only. speed=0 cancels the alarm.
    The alarm will be sent from the tracker each 30 seconds until the speed is lower
    than the speed set.
    Internally, the speed is sent in tens of kilometers. 1 = 10 km/h, 0A = 100 km/h.
    The max speed limit is two bytes long: FF = 255 (which is too much, but test for sanity check.)
    Format: @@<L><ID><0x4105><speed (1 byte in hex code)><checksum>

    Returns the encoded command in hex string.
    """
    try:
        speed_int = int(speed)/10
        if speed_int > 255:
            speed_int = 255 #downgrade it.
        if speed_int < 10:
            speed_int = 1 # minimum speed is 10 km/h -> 1
        speed_hex = hex(speed_int)
        speed_hex = speed_hex.replace('0x','').upper()
        if speed_int < 16:
            speed_hex = '0%s'%speed_hex
        speed = speed_hex
    except:
        print 'ERROR: The speed does not represents an integer.'
        return None
    while len(sender) < 14:
            sender = sender+'F'
    new_data = '40400000%s4105%s' % (sender, speed)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data # @ = 0x40
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_initialization(sender):
    """
    Encodes the initialization command 
    (reset to factory settings, except for the password, IP, Port, APN, ID and GPRS interval)
    Format: @@<L><ID><0x4110><checksum>\r\n
    """
    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4110' % (sender)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'Initialization Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_sleepMode(sender, level):
    """
    Encondes the sleep mode command, the command sets the level of sleep.
    level are 1/2/3, in string, interger values only. 
    If tracker gets continuous GPS fixes for 32 times or NO GPS Fix for 128 times, the tracker will sleep for:
    Level 1: sleeps for 64 seconds.
    Level 2: sleeps for 128 seconds.
    Level 3: sleeps for 192 seconds.
    After this, the tracker will wake up and sleep periodically.

    Format: @@<L><ID><0x4113><level (1 byte in hex code)><checksum>

    Returns the encoded command in hex string.
    """
    try:
        level_int = int(level)
        if level_int > 3:
            level_int = 3 #downgrade it.
        if level_int < 1:
            level_int = 1 #Minimum level is 1
        level_hex = hex(level_int)
        level_hex = level_hex.replace('0x','').upper()
        level_hex = '0%s'%level_hex
        level = level_hex
    except:
        print 'ERROR: The level does not represents an integer.'
        return None
    while len(sender) < 14:
            sender = sender+'F'
    new_data = '40400000%s4113%s' % (sender, level)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data # @ = 0x40
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'Sleep mode command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_powerDown(sender, time):
    """
    Encodes the Power DOWN command (4126)
    The device enters in spleep mode after time minutes of inactivity or stationary. GPS and GPRS stop working.
    Any message, call, movement, or input change wakes up the device.
    The time is in minutes. 0=off, [1..99]
    Format: @@<L><ID><0x4126><data><checksum>\r\n
    """
    #check the ranges!
    strTime = ''
    for char in time:
        strTime += '3%s'%char
    time = strTime

    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4126%s' % (sender, time)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'PowerDown Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_voiceMonitoringAuth(sender, phone):
    """
    Encodes the Listen-in authorization command (4130)
    Sets the authorized phone number for voice monitoring
    The phone is 16 digits number max, and can starting with a +
    
    Format: @@<L><ID><0x4130><data><checksum>\r\n
    """
    if len(phone) > 15:
        print 'PHONE NUMBER EXCEEDS THE 16 digits.'
        return None

    if phone[0] == '+':
        phone = phone.replace('+', '')
        hasPlus = True
    else:
        hasPlus = False
    strPhone = ''
    for char in phone:
        strPhone += '3%s'%char
    phone = strPhone
    if hasPlus:
        phone = '2B%s'%phone # + -> 0x2B

    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4130%s' % (sender, phone)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'VoiceMonitoring Auth Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_setTrembleSensorSensitivity(sender, s):
    """
    Encodes the set Tremble Sensor Sensitivity command (4135)
    Sensitivity of tremble sensor is the key parameter for sleep mode, wake up and tow alarm etc.
    Data = [1,255]. Default is 30.
    Format: @@<L><ID><0x4135><data><checksum>\r\n
    """
    
    try:
        if int(s) > 255:
            s = '255'
    except:
        print 'ERROR when converting sensitivity.'
        s = '30' #default

    strSens = ''
    for char in s:
        strSens += '3%s'%char
    s = strSens

    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4135%s' % (sender, s)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'SET Tremble Sensor Sensitivity Auth Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_setGPSAntennaCut(sender, val):
    """
    ONLY FOR VT400
    Encodes the set GPS Antenna Cut command (4150)
    Data = 0: Disable, Data =1: Enable
    Format: @@<L><ID><0x4150><data><checksum>\r\n
    Data is 00 or 01 (not 31, 30)
    """
    
    try:
        if int(val) > 1:
            val = '1'
    except:
        print 'ERROR when converting sensitivity.'

    strVal = ''
    for char in val:
        strVal += '0%s'%char
    val = strVal

    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4150%s' % (sender, val)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'SET GPS Antenna CUT Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data


def encode_cmd_setTrackByDistance(sender, val):
    """
    Encodes the set Track by Distance command (4303)
    Data 0: Cancel, [1,4294967295] meters. Recommended values above 300.
    Format: @@<L><ID><0x4303><data><checksum>\r\n
    """
    
    strVal = ''
    for char in val:
        strVal += '3%s'%char
    val = strVal

    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4303%s' % (sender, val)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'SET Track By Distance Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data



def encode_cmd_deleteMileage(sender):
    """
    Encodes the delete Mileage command (4351)
    When mileage is deleted, the server should have a corresponding program to avoid calculation mistake.
    Format: @@<L><ID><0x4351><checksum>\r\n
    """
    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4351' % (sender)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'Delete Mileage Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data

def encode_cmd_rebootGPS(sender):
    """
    
    WARNING: This method does not pass the test. The checksum is different from execution that from the example at docs.
    ********************************************************************************************************************

    Encodes the Reboot GPS command (4902)
    Reboots the GPS module.
    Format: @@<L><ID><0x4902><checksum>\r\n
    """
    while len(sender) < 14:
            sender += 'F'
    new_data = '40400000%s4902' % (sender)  #stills missing the checksum and lenght.
    new_len = hex(len(new_data+'00000d0a')/2).replace('0x', '').upper() #this includes a 0000 checksum and the ending delimiter.
    while len(new_len) < 4:
        new_len = '0'+new_len
    new_data = new_data.replace('40400000', '',1,1) #remove the first characters to insert the new len.
    new_data = '4040'+new_len+new_data
    try:
        crc16ccitt = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        new_checksum = hex(crc16ccitt(binascii.a2b_hex(new_data)))
        new_checksum = new_checksum.replace('0x', '').upper()
    except (ValueError, TypeError):
        print 'Error on value for the new checksum. Message will have an invalid CRC!'
        #FIXME: We need to know what is happening exactly here, to be able to fix the anomaly.
        new_checksum = '0000'
    new_data = new_data + new_checksum #the delimiter is added automatically.

    print 'Reboot GPS Command: %s'%new_data

    #encode data as hex. This is the trick to send the correct data!
    new_data = ''.join(list( hex2ascii(new_data) ))

    return new_data

