
import datetime

from twisted.internet import defer
from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver

from twisted.internet import reactor
import argparse

#Logs: everything written to stdout (eg print) and stderr will be logged.
from twisted.python import log
from twisted.python.logfile import DailyLogFile

class TrackerProtocol(LineReceiver):

    def connectionMade(self):
        self.sendLine("2424001145221520187FFF50006E510D0A") # a login cmd.
        #I Still need a commands pool, with differente commands to test and different data (sender, location, date-time...)

    def connectionLost(self, reason):
        # print 'CLOSED CONNECTION @ %s'%(datetime.datetime.now())
        self.factory.all_finished()

    def lineReceived(self, data):
        print 'DATA RECEIVED: %s @ %s'%(data,datetime.datetime.now())
        


class TrackerClientFactory(ClientFactory):

    protocol = TrackerProtocol

    def __init__(self, deferred):
        self.deferred = deferred
        #log.startLogging(DailyLogFile.fromFullPath("stress.log"))
        #log.msg('STARTING TECOLOTE SERVER!')

    def all_finished(self):
        #print 'ALL FINISHED Deferred is %s'%self.deferred
        if self.deferred is not None:
            d, self.deferred = self.deferred, None
            d.callback("finished")

    def clientConnectionFailed(self, connector, reason):
        print 'Connection Failed :%s'%reason
        if self.deferred is not None:
            d, self.deferred = self.deferred, None
            d.errback(reason)



def connect2Server(host, port):
    d = defer.Deferred()
    factory = TrackerClientFactory(d)
    reactor.connectTCP(host, port, factory)
    return d


def stress_main():
    parser = argparse.ArgumentParser()
    parser.add_argument("host", help="Connect to the specified host")
    parser.add_argument("port", help="Connect to the specified port", type=int)
    parser.add_argument("num", help="The STRESS NUM - Number of commands to send.", type=int)
    args = parser.parse_args()

    responses = []
    errors = []
    STRESS_NUM = args.num
    log.startLogging(DailyLogFile.fromFullPath("stress.log"))
    log.msg('STARTING TECOLOTE STRESSER!')


    def con_done(_):
        print 'CON_DONE @ %s'%(datetime.datetime.now())
        responses.append("ok")

    def con_failed(err):
        print 'CON_ERROR: %s'%err
        errors.append(err)

    def all_done(_):
        if len(responses) + len(errors) >= args.num:
            reactor.stop()
            elapsed = datetime.datetime.now() - start_time
            print 'ELAPSED TIME: %s.'%(elapsed)


    start_time = datetime.datetime.now()

    #This stills make the test 'sync' in a single thread. I want to simulate multiple concurrent 'users'.
    # 'sync' -> one after another; But not blocking while waiting for a response (using a deferred).
    while STRESS_NUM > 0:
        d = connect2Server(args.host, args.port)
        d.addCallbacks(con_done, con_failed)
        d.addBoth(all_done)
        STRESS_NUM -= 1

    reactor.run()



if __name__ == '__main__':
    stress_main()
